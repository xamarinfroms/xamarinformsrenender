﻿
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using XamarinFormsRenender;
using XamarinFormsRenender.iOS;

[assembly: ExportRenderer(typeof(MyEntry), typeof(MyEntryRenender))]
namespace XamarinFormsRenender.iOS
{
    public  class MyEntryRenender: EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
            {
                this.Control.Layer.CornerRadius = 0;
                this.Control.Layer.BorderWidth = 1;
                this.Control.Layer.BorderColor = UIColor.Gray.CGColor;
            }
        }
    }
}
