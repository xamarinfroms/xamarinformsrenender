﻿using Android.Content;
using Android.Graphics.Drawables;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XamarinFormsRenender.Droid;
using XamarinFormsRenender;
[assembly: ExportRenderer(typeof(MyEntry), typeof(MyEntryRenender))]
namespace XamarinFormsRenender.Droid
{
    public class MyEntryRenender : EntryRenderer
    {
        public MyEntryRenender(Context context) : base(context) { }

    
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
            {
                var density = this.Context.Resources.DisplayMetrics.Density;
                GradientDrawable drawable = new GradientDrawable();
                drawable.SetStroke((int)(1 * density),Android.Graphics.Color.Gray);
                this.Control.SetBackground(drawable);
            }
        }

    }
}